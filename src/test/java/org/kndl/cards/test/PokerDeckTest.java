package org.kndl.cards.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.kndl.cards.Deck;
import org.kndl.cards.poker.PokerDeck;
import org.kndl.cards.exception.NoCardsOnDeckException;
import org.kndl.cards.model.Card;
import org.kndl.cards.shuffle.impl.RandomCardShuffleStrategy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 *
 *
 */
public class PokerDeckTest {

    private Deck deck;

    @Before
    public void before() {
        this.deck = new PokerDeck();
    }

    /**
     * Check for an uninitialized deck (via initialize()) and a
     * subsequent initialization
     */

    @Test
    public void deckUninitalizedTest() {
        try {
            Card card = deck.dealOneCard();
            Assert.fail("NoCardsOnDeckException: should be thrown.");
        } catch (NoCardsOnDeckException e) {
            //e.printStackTrace();
        }
        assertFalse(deck.isInitialized());
        deck.initialize();
        assertTrue(deck.isInitialized());
    }

    /**
     * Check for shuffling working with no shufflers
     */

    @Test
    public void deckShuffleNoShufflersTest() {
        deck.shuffle();
        try {
            Card card = deck.dealOneCard();
            // should be the first enum value for both the suit and value
            assertEquals(card.getSuit().ordinal(), 0);
            assertEquals(card.getValue().ordinal(),0);
        } catch (NoCardsOnDeckException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check for shuffling working with one shuffler (using the random
     * shuffler)
     */

    @Test
    public void deckShuffle1ShufflerTest() {
        deck.addShuffler(new RandomCardShuffleStrategy());
        deck.initialize();
        deck.shuffle();
        try {
            Card card = deck.dealOneCard();
        } catch (NoCardsOnDeckException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check for consistent deals.
     */

    @Test
    public void deckShuffleDealTest() {
        deck.addShuffler(new RandomCardShuffleStrategy());
        deck.initialize();
        deck.shuffle();
        int deckSize = deck.size();
        int cardsDealt = 0;
        boolean outOfCards = false;
        while (!outOfCards) {
            try {
                Card c = deck.dealOneCard();
                cardsDealt++;
            } catch (NoCardsOnDeckException e) {
                outOfCards = true;
            }
        }
        assertEquals(deckSize, cardsDealt);
    }

    /**
     * Check for correct peek functionality
     */
    @Test
    public void deckPeekTest() {
        deck.initialize();
        assertTrue(deck.peek(52));
        assertFalse(deck.peek(53));
        try {
            Card c = deck.dealOneCard();
            assertTrue(deck.peek(51));
            assertFalse(deck.peek(52));
        } catch (NoCardsOnDeckException e) {
            e.printStackTrace();
        }
    }
}

package org.kndl.cards.initializer;

import org.kndl.cards.DeckInitializer;
import org.kndl.cards.model.Card;

/**
 *
 *
 *
 */
public class TestDeckInitializer implements DeckInitializer {
    @Override
    public Card[] createDeck() {
        return new Card[0];
    }
}

package org.kndl.cards.shuffle.impl.test;

import org.junit.Before;
import org.junit.Test;
import org.kndl.cards.model.Card;
import org.kndl.cards.shuffle.impl.RandomCardShuffleStrategy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Tests for random card shuffling.
 *
 * @author Shepherd Kendall
 *
 */
public class RandomCardShuffleStrategyTest extends AbstractShuffleStrategyTest {

    @Before
    public void before() {
        setShuffler(new RandomCardShuffleStrategy());
    }

    /**
     * Difficult to test the accuracy, so we verify that the values in the
     * array are within the limitations of the deck's suit and value set
     */

    @Test
    public void testSuccess() {
        int[] deck = new int[]{0,1,2,3};
        //int[] shuffled = getShuffler().shuffle(deck);
        //assertEquals(shuffled.length,4);
    }

    /**
     * Test for empty or null deck (should be treated the same).
     */

    @Test
    public void testEmptyNullDeck() {
        Card[] shuffled = getShuffler().shuffle(null);
        assertNotEquals(shuffled,null);
        assertEquals(shuffled.length,0);
    }

}

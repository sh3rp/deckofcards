package org.kndl.cards.shuffle.impl.test;


import org.junit.Before;
import org.junit.Test;
import org.kndl.cards.shuffle.impl.HalfCutShuffleStrategy;

/**
 * Testing the half cut strategy.
 *
 *
 */
public class HalfCutShuffleStrategyTest extends AbstractShuffleStrategyTest {

    @Before
    public void before() {
       setShuffler(new HalfCutShuffleStrategy());
    }

    /**
     * Test an even shuffle.
     */

    @Test
    public void testSuccessfulEvenShuffle() {
        //int[] shuffled = getShuffler().shuffle(new int[] {0,1,2,3,4,5});
        //isShuffled(shuffled,new int[]{3,4,5,0,1,2});
    }

    /**
     * Test an odd shuffle.
     */

    @Test
    public void testSuccessfulOddShuffle() {
        //int[] shuffled = getShuffler().shuffle(new int[] {0,1,2,3,4,5,6});
        //isShuffled(shuffled,new int[]{6,3,4,5,0,1,2});
    }

    /**
     * Test for empty or null deck (should be treated the same).
     */

    @Test
    public void testEmptyNullDeck() {
        //int[] shuffled = getShuffler().shuffle(null);
        //assertNotEquals(shuffled,null);
        //assertEquals(shuffled.length,0);
    }
}

package org.kndl.cards.shuffle.impl.test;

import org.junit.Assert;
import org.kndl.cards.initializer.TestDeckInitializer;
import org.kndl.cards.model.Card;
import org.kndl.cards.shuffle.ShuffleStrategy;

import static org.junit.Assert.assertEquals;

/**
 * Used as a base class for shuffling strategy tests.
 *
 * @author Shepherd Kendall
 *
 */
public abstract class AbstractShuffleStrategyTest {

    /**
     * Our shuffler
     */

    private ShuffleStrategy shuffler;

    protected Card[] deck;

    public ShuffleStrategy getShuffler() {
        return shuffler;
    }

    public void setShuffler(ShuffleStrategy shuffler) {
        this.shuffler = shuffler;
    }

    public void initializeDeck() {
        this.deck = new TestDeckInitializer().createDeck();
    }

    public Card[] getDeck() {
        return this.deck;
    }

    /**
     * Verifies that the shuffled deck is the same as the
     * expected deck.
     *
     * @param shuffled
     * @param expected
     */

    public void isShuffled(Card[] shuffled, Card[] expected) {
        if(shuffled == null || expected == null)
            Assert.fail("Null comparisons.");
        if(shuffled.length != expected.length)
            Assert.fail("Comparisons are not of equal length.");
        for(int i=0;i<shuffled.length;i++)
            assertEquals(shuffled[i], expected[i]);
    }
}

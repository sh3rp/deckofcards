package org.kndl.cards.shuffle.impl.test;

import org.junit.Before;
import org.junit.Test;
import org.kndl.cards.shuffle.impl.RiffleShuffleStrategy;

/**
 * Tests for riffle shuffle strategy.
 *
 * @author Shepherd Kendall
 *
 */
public class RiffleShuffleStrategyTest extends AbstractShuffleStrategyTest {

    @Before
    public void before() {
        setShuffler(new RiffleShuffleStrategy());
    }

    /**
     * Test for an even card count.
     */

    @Test
    public void testSuccessEvenCardCount() {
        //int[] shuffled = getShuffler().shuffle(new int[] {0,1,2,3,4,5});
        //isShuffled(shuffled,new int[]{0,3,1,4,2,5});
    }

    /**
     * Test for an odd card count.
     */

    @Test
    public void testSuccessOddCardCount() {
        //int[] shuffled = getShuffler().shuffle(new int[] {0,1,2,3,4,5,6});
        //isShuffled(shuffled,new int[]{0,3,1,4,2,5,6});
    }

    /**
     * Test for empty or null deck (should be treated the same).
     */

    @Test
    public void testEmptyNullDeck() {
        //int[] shuffled = getShuffler().shuffle(null);
        //assertNotEquals(shuffled,null);
        //assertEquals(shuffled.length,0);
    }

}

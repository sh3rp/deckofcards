package org.kndl.cards.shuffle;

import org.kndl.cards.model.Card;

/**
 * The API contract used to perform the actual shuffling of
 * the cards.  When implemented, the contract will take in a
 * deck of cards (integer array) and perform some level of
 * shuffling of that deck.
 *
 * The resulting array may or may not be the same object
 * returned, depending on the strategy implemented for
 * performing the shuffle.  This may or may not be seen as
 * a design flaw, since shuffling may get memory intensive
 * if performed sequentially very quickly, with many
 * different shufflers.
 *
 * @author Shepherd Kendall
 *
 *
 */
public interface ShuffleStrategy {

    /**
     * Implementer must accept an integer array representing
     * card digits and perform some sort of shuffling on
     * those integers in the array.
     *
     * @param cards
     * @return
     */

    public Card[] shuffle(Card[] cards);
}

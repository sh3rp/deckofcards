package org.kndl.cards.shuffle.impl;

import org.kndl.cards.model.Card;
import org.kndl.cards.shuffle.ShuffleStrategy;

/**
 * Standard riffle shuffle.  This strategy creates a new deck by
 * splitting the deck into two distinct groups and interleaving
 * cards from each group.
 *
 * @author Shepherd Kendall
 *
 */
public class RiffleShuffleStrategy implements ShuffleStrategy {

    @Override
    public Card[] shuffle(Card[] cards) {

        if(cards == null)
            return new Card[0];

        // create a new pivot

        int pivot = cards.length / 2;
        Card[] temp = new Card[cards.length];
        int curPos = 0;
        for(int i=0;i<pivot;i++) {
            temp[curPos++] = cards[i];
            temp[curPos++] = cards[(pivot+i)];
        }

        if(cards.length % 2 > 0)
            temp[curPos] = cards[cards.length-1];
        return temp;
    }
}

package org.kndl.cards.shuffle.impl;

import org.kndl.cards.model.Card;
import org.kndl.cards.shuffle.ShuffleStrategy;

import java.util.Random;

/**
 * Standard random shuffle strategy.  The strategy iterates over the
 * deck, selecting a random position to move the card to.  If the
 * randomizer selects a position that is where the card already
 * exists in the deck, a random number is generated until that
 * position is no longer the swap position.  From an implementation
 * perspective, this has potential to deadlock, but this is
 * statistically and practically impossible.
 *
 * If a constructor value {@code iterations} is passed, that number
 * of shuffles will be performed.  The default number of iterations
 * is 1.
 *
 * @author Shepherd Kendall
 *
 */
public class RandomCardShuffleStrategy implements ShuffleStrategy {

    /**
     * Used to generate random integers for card swapping
     */

    private Random                          randomizer = new Random();

    /**
     * Number of iterations to run the shuffle
     */

    private int                             iterations = 0;

    /**
     * Defaults to 1 iteration of the shuffle
     */

    public RandomCardShuffleStrategy() {
        this(1);
    }

    /**
     * Specify a custom number of shuffling iterations.
     *
     * @param iterations
     */

    public RandomCardShuffleStrategy(int iterations) {
        this.iterations = iterations;
    }

    @Override
    public Card[] shuffle(Card[] cards) {

        if(cards == null)
            return new Card[0];

        for (int iteration = 0; iteration < iterations; iteration++) {

            // generate a new seed each time we iterate to increase
            // randomness

            randomizer.setSeed(System.currentTimeMillis());

            for (int i = 0; i < cards.length; i++) {

                // get a random index that isn't the current index

                int randIdx = i;
                while ((randIdx = Math.abs(randomizer.nextInt(cards.length))) == i) ;

                // swap cards

                Card temp1 = cards[i];
                cards[i] = cards[randIdx];
                cards[randIdx] = temp1;

            }
        }

        return cards;
    }
}

package org.kndl.cards.shuffle.impl;

import org.kndl.cards.model.Card;
import org.kndl.cards.shuffle.ShuffleStrategy;

/**
 * Cuts the deck into halves.  If the deck count is odd, we add
 * the last card to the front of the newly shuffled deck.  Then
 * do a bulk move of the back set of cards to the front and
 * vice versa.
 *
 * @author Shepherd Kendall
 *
 */
public class HalfCutShuffleStrategy implements ShuffleStrategy {

    @Override
    public Card[] shuffle(Card[] cards) {

        if(cards == null)
            return new Card[0];

        Card[] temp = new Card[cards.length];

        // grab the number of cards to swap

        int bulkSize = cards.length / 2;

        // does the deck have an odd deck count?

        int startIdx = cards.length % 2;

        // if we have an odd deck count, move the last card to
        // the front of the deck

        if(startIdx > 0)
            temp[0] = cards[cards.length-1];

        // do our card swap

        for(int i=0;i<bulkSize;i++) {
            temp[startIdx+i] = cards[i+bulkSize];
            temp[startIdx+(i+bulkSize)] = cards[i];
        }

        return temp;
    }

}

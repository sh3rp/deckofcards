package org.kndl.cards.poker;

/**
 *
 *
 *
 */
public enum PokerCardValue {

    ACE("\u0041"),
    TWO("\u0032"),
    THREE("\u0033"),
    FOUR("\u0034"),
    FIVE("\u0035"),
    SIX("\u0036"),
    SEVEN("\u0037"),
    EIGHT("\u0038"),
    NINE("\u0039"),
    TEN("\u0031\u0030"),
    JACK("\u004A"),
    QUEEN("\u0051"),
    KING("\u004b");

    private String UTFCode;

    PokerCardValue(String UTFCode) {
        this.UTFCode = UTFCode;
    }

    public String getCode() {
        return this.UTFCode;
    }
}

package org.kndl.cards.poker;

import org.kndl.cards.Deck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * All definitions related to the game of poker are in the
 * implementation of the {@link org.kndl.cards.Deck} abstraction.
 * <p/>
 * The card bit mapping is represented as follows:
 * <p/>
 * - Card value is bits 0-3
 * - PokerSuit value is bits 5-7
 * <p/>
 * Suits are defined on the fly at instantiation, but this is
 * mitigated by making suits immutable.
 */
public class PokerDeck extends Deck {

    private static final Logger LOGGER = LoggerFactory.getLogger(PokerDeck.class);

    public PokerDeck() {
        super(new PokerInitializer());
    }

}

package org.kndl.cards.poker;

import org.kndl.cards.DeckInitializer;
import org.kndl.cards.model.Card;

/**
 *
 *
 *
 */
public class PokerInitializer implements DeckInitializer {
    @Override
    public Card[] createDeck() {
        Card[] cards = new Card[PokerSuit.values().length * PokerCardValue.values().length];

        int idx = 0;

        for(PokerSuit suit : PokerSuit.values())
            for(PokerCardValue value : PokerCardValue.values())
                cards[idx++] = new PokerCard(suit,value);


        return cards;
    }
}

package org.kndl.cards.poker.example;

import org.kndl.cards.Deck;
import org.kndl.cards.exception.NoCardsOnDeckException;
import org.kndl.cards.model.Card;
import org.kndl.cards.poker.PokerDeck;
import org.kndl.cards.shuffle.impl.HalfCutShuffleStrategy;
import org.kndl.cards.shuffle.impl.RandomCardShuffleStrategy;
import org.kndl.cards.shuffle.impl.RiffleShuffleStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Reference implementation for the poker part of the card
 * dealing framework.
 *
 * @author Shepherd Kendall
 */
public class PokerBot {

    /**
     * Logger for console output
     */

    public static final Logger LOGGER = LoggerFactory.getLogger(PokerBot.class);

    public static void main(String args[]) {

        // create our card deck

        Deck deck = new PokerDeck();
        deck.initialize();

        // add our shufflers

        deck.addShuffler(new RandomCardShuffleStrategy());
        deck.addShuffler(new RiffleShuffleStrategy());
        deck.addShuffler(new HalfCutShuffleStrategy());

        // perform the shuffle; this also resets the deck

        deck.shuffle();

        // how many cards to deal out per hand

        int handSize = 5;

        // do we have more cards to deal?

        boolean canDeal;

        while (canDeal = deck.peek(handSize)) {

            Card[] fiveCards = new Card[handSize];

            for (int i = 0; i < fiveCards.length; i++) {
                try {
                    fiveCards[i] = deck.dealOneCard();
                } catch (NoCardsOnDeckException e) {
                    // should never hit this with peek
                    // log an error if we do though, just to make sure
                    LOGGER.error("NO CARDS EXCEPTION, RECHECK YOUR CODE");
                }
            }
            LOGGER.info("Your hand: {} {} {} {} {}",
                    fiveCards[0], fiveCards[1], fiveCards[2], fiveCards[3], fiveCards[4]);
        }
        LOGGER.info("Cannot deal {} cards, aborting.", handSize);
    }
}

package org.kndl.cards.poker;

public enum PokerSuit {

    HEART("\u2665"),
    DIAMOND("\u2666"),
    CLUB("\u2663"),
    SPADE("\u2660");

    private final String UTFCode;

    PokerSuit(String UTFCode) {
        this.UTFCode = UTFCode;
    }

    public String getCode() {
        return this.UTFCode;
    }
}

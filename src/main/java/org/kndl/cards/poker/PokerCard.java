package org.kndl.cards.poker;

import org.kndl.cards.model.Card;

/**
 *
 *
 *
 */
public class PokerCard extends Card<PokerSuit,PokerCardValue> {

    public PokerCard(PokerSuit suit, PokerCardValue value) {
        super(suit, value);
    }

    /**
     * Convenience method for outputting the card's suit
     * and face value.
     *
     * @return
     */

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getSuit().getCode()).append(getValue().getCode());
        return sb.toString();
    }
}

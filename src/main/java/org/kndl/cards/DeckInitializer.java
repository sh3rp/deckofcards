package org.kndl.cards;

import org.kndl.cards.model.Card;

/**
 * Facade for creating a deck initialization.
 *
 * @author Shepherd Kendall
 *
 *
 */
public interface DeckInitializer {

    /**
     * Create an initialized deck based on the type of deck
     * you'd like to create.  The underlying implementation
     * decides what cards to use and the initial position of
     * all the cards.
     *
     * @return Card[]
     */

    public Card[] createDeck();
}

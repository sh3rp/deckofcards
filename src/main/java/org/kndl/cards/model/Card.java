package org.kndl.cards.model;

/**
 * Data transfer object used for easy manipulation of
 * card related data.
 * <p/>
 * This might eventually be persistable into a DB of
 * some kind either through Hibernate or some other
 * ORM.
 * <p/>
 * The card is implemented assuming a suit and value
 * enum combination.  This allows for various suit and
 * value definitions (i.e. Old Maid, Uno, etc.).
 * </p>
 *
 * @author Shepherd Kendall
 */
public abstract class Card<S extends Enum, V extends Enum> {

    /**
     * The suit object associated with this card instance
     */

    private S suit;

    /**
     * Face value of the card
     */

    private V value;

    public Card(S suit, V value) {
        this.suit = suit;
        this.value = value;
    }

    /**
     * Getter for suit object.
     *
     * @return
     */

    public final S getSuit() {
        return this.suit;
    }

    /**
     * Getter for card face value.
     *
     * @return
     */

    public final V getValue() {
        return this.value;
    }

    public int hashCode() {
        return (getSuit().hashCode() + getValue().hashCode());
    }

    public boolean equals(Card<S,V> card) {
        return card.getSuit().equals(getSuit()) && card.getValue().equals(getValue());
    }
}

package org.kndl.cards;

import org.kndl.cards.exception.NoCardsOnDeckException;
import org.kndl.cards.model.Card;
import org.kndl.cards.shuffle.ShuffleStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Wrapper that maintains a deck of cards based on Card
 * objects.  The underlying implementation is provided
 * an implementation for creating the initial deck (typically
 * an ordered array of cards).
 *
 * @author Shepherd Kendall
 */
public abstract class Deck {

    private static final Logger LOGGER  = LoggerFactory.getLogger(Deck.class);

    /**
     * Object representation of the actual deck of cards; make
     * it volatile so we get the most up-to-date non-cached
     * version
     */

    protected volatile Card[] deck;

    /**
     * Position of the dealer in the deck; when this index
     * reaches the length of the deck, no more cards may
     * be dealt from the deck; volatile so we get the most
     * update-to-date non-cached version
     */

    protected volatile int currentDeckPosition = 0;

    /**
     * List of shuffle strategies through which to iterate
     * and pass the deck.  Each shuffle strategy has it's
     * strengths and weaknesses and so, as in the real world
     * it makes sense to use any combination of any number
     * of shuffling strategies together.  Three default
     * shuffle strategies are implemented in this framework:
     * <p/>
     * - BulkMove: This swaps a block of cards with another
     * block of cards and repeats until all cards
     * groups have been swapped with another; if
     * the number of cards in the deck are uneven,
     * the remainder cards are moved from the back
     * to the front of the deck
     * <p/>
     * - Random:   Card positions are swapped at random
     * <p/>
     * - Riffle:   Standard riffle shuffle using a secondary
     * array a replacement for the deck array
     * being shuffled
     */

    private List<ShuffleStrategy> shufflers;

    /**
     * Locking objects that allow multiple threads to pull cards
     * from the deck and initialize the deck without interfering
     * with other threads.
     * <p/>
     */

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock readLock = lock.readLock();
    private final Lock writeLock = lock.writeLock();

    private DeckInitializer initializer;

    public Deck(DeckInitializer initializer) {
        this.shufflers = new ArrayList<>();
        this.initializer = initializer;
    }

    /**
     * Adds a shuffle strategy to the list of shufflers.
     *
     * @param shuffler
     */

    public void addShuffler(ShuffleStrategy shuffler) {
        writeLock.lock();
        try {
            shufflers.add(shuffler);
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * Removes a shuffle strategy from the list of shufflers.
     *
     * @param shuffler
     */

    public void removeShuffler(ShuffleStrategy shuffler) {
        writeLock.lock();
        try {
            shufflers.remove(shuffler);
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * Engages the lock to allow for deck initialization.
     */

    public void initialize() {
        writeLock.lock();
        try {
            this.deck = initializer.createDeck();
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * The size of a deck if it's defined.  If it the deck is
     * uninitialized, returns 0.
     *
     * @return
     */

    public int size() {
        return isInitialized() ? deck.length : 0;
    }

    /**
     * True if the deck integer array has been created, false if not.
     *
     * @return
     */

    public boolean isInitialized() {
        return deck != null && currentDeckPosition < deck.length;
    }

    /**
     * Initializes the deck then passes it through any shuffling
     * strategies that have been added.
     */

    public void shuffle() {

        if (LOGGER.isDebugEnabled())
            LOGGER.debug("Initializing card deck.");

        writeLock.lock();

        try {
            for (ShuffleStrategy shuffler : shufflers) {

                if (LOGGER.isDebugEnabled())
                    LOGGER.debug("Executing shuffler: {}", shuffler.getClass().getCanonicalName());

                long start = System.nanoTime();

                this.deck = shuffler.shuffle(this.deck);

                long stop = System.nanoTime();

                if (LOGGER.isDebugEnabled())
                    LOGGER.debug("Shuffle took {}ms.", ((double) ((stop - start) / 1000)));

            }
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * Deals a card off the top of the deck.  If no cards can be
     * dealt, a {@link org.kndl.cards.exception.NoCardsOnDeckException} is
     * thrown.
     * <p/>
     *
     * @return
     * @throws NoCardsOnDeckException
     */

    public Card dealOneCard() throws NoCardsOnDeckException {

        writeLock.lock();                                    // lock the deck so we can increment the counter

        try {
            if (deck == null ||                              // deck hasn't been init'ed
                    currentDeckPosition >= deck.length) {    // exceeded number of cards on deck

                throw new NoCardsOnDeckException();
            }

            // otherwise, return the card object at this position
            // card objects are defined by the deck implementation

            return deck[currentDeckPosition++];
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * Allows you to peek ahead by {@code numCards} to see that
     * number of cards is available to fetch from the deck.
     * <p/>
     * Returns true if the number of cards specified are available
     * and false if the number of cards left to deal is less than
     * the specified number.
     *
     * @param numCards
     * @return
     */

    public boolean peek(int numCards) {
        readLock.lock();
        try {
            return isInitialized() && numCards <= deck.length - currentDeckPosition;
        } finally {
            readLock.unlock();
        }
    }

}

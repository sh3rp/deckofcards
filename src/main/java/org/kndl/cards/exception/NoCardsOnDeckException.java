package org.kndl.cards.exception;

/**
 * Triggers the event that no other cards may be dealt from
 * the deck.
 *
 * @author Shepherd Kendall
 *
 */
public class NoCardsOnDeckException extends Throwable {
}

# DeckOfCards - V1.0

This is a library which provides a framework for efficiently generating
a deck of gaming cards.  The framework provides for a semi-generic method
of managing the cards, as well as providing reference implementations
for the game of poker.

## Quick Start

To try out the reference implementation, make sure you have Gradle 1.0
or greater installed.  Then run the command on a *nix/OSX based machine:

> gradle build ; java -jar build/libs/deckofcards-1.0.jar

## Framework Usage

The framework provides some key components that allows for easy
implementation of a gaming deck of cards.  These components are generic
enough that you need only define a few objects to use them.  For example,
the poker implementation defines a poker deck by defining:

- A suit Enum
- A value Enum
- A card, containing the suit and value Enum's
- A deck

Using these definitions, one can create any type of gaming deck and 
easily do card manipulations.

Additionally, the framework provides a simple API for doing card shuffles
using the ShuffleStrategy interface.  By combining a list of shuffle
strategies, the deck can generate a highly randomized order of cards 
in the deck.

Sample implementations are found in the RandomShuffleStrategy,
BiffleShuffleStrategy, and the HalfCutShuffleStrategy.  A deck can have
any number of any instance of these strategies attached to it and the
shuffle strategies themselves are left to their own implementations 
internally for the number of shufflable cards.

## Example

Here's a simple example that demonstrates how easy it is to use the
gaming deck.

    // initialize the deck
    Deck deck = new PokerDeck();
    deck.initialize();
    // add our shufflers
    deck.addShuffler(new RandomCardShuffleStrategy());
    deck.addShuffler(new RiffleShuffleStrategy());
    deck.addShuffler(new HalfCutShuffleStrategy());
    // perform the shuffle; this also resets the deck
    deck.shuffle();
    try {
        Card card = deck.dealACard();
    } catch(NoCardsOnDeckException ncode) {
        e.printStackTrace();
    }
